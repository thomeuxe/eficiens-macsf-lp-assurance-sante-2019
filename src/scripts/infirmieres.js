import "./common";
import $ from "jquery";
import {
  TimelineMax,
  TweenMax,
  Linear
} from 'gsap';
import ScrollMagic from 'scrollmagic';
import 'animation.gsap';

$(document).ready(function() {
  /**
   * Hero animation
   */

  TweenMax.defaultEase = Linear.easeNone;
  var controller = new ScrollMagic.Controller();
  var tl = new TimelineMax();
  tl.fromTo("#hero-background-infirmieres #depth1", 1, {
    y: "+=40",
    force3D: true
  }, {
    y: "+=-170"
  }, 0);

  tl.fromTo("#hero-background-infirmieres #depth2, #hero-background-infirmieres #depth2bis", 1, {
    y: "+=20",
    force3D: true
  }, {
    y: "+=-50"
  }, 0);

  tl.fromTo("#hero-background-infirmieres #depth3", 1, {
    y: "+=0",
    force3D: true
  }, {
    y: "+=-20"
  }, 0);

  tl.to("#heroTitle", 1, {
    y: "+=-50",
    force3D: true
  }, 0);

  var scene = new ScrollMagic.Scene({
      triggerElement: "body",
      duration: "80%",
      triggerHook: 0
    })
    .setTween(tl)
    .addTo(controller);
});
