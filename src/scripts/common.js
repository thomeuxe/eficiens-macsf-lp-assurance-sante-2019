import "../styles/index.scss";
import $ from "jquery";
import "slick-carousel";

window.jQuery = $;
window.$ = $;

$(document).ready(function() {
  $(".formules [data-slider]").slick({
    infinite: true,
    slidesToShow: 3,
    centerMode: true,
    centerPadding: '0px',
    responsive: [{
        breakpoint: 968,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          adaptiveHeight: true
        }
      }
    ]
  });

  $(".remboursements [data-slider]").slick({
    infinite: true,
    slidesToShow: 3,
    responsive: [{
        breakpoint: 968,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          adaptiveHeight: true
        }
      }
    ]
  });

  setTimeout(function() {
    var maxHeight = Math.max.apply(
      null,
      $(".js-formule-card-title")
      .map(function() {
        return $(this).height();
      })
      .get()
    );

    $(".js-formule-card-title").css("height", maxHeight);

    var maxHeight = Math.max.apply(
      null,
      $(".js-formule-card-text")
      .map(function() {
        return $(this).height();
      })
      .get()
    );

    $(".js-formule-card-text").css("height", maxHeight);
  }, 100);

  /**
  * Footer store links
  */

  var isIOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
  var isAndroid = navigator.userAgent.toLowerCase().indexOf("android") > -1;

  if(isIOS) {
    $('.js-store-android').hide();
  } else if(isAndroid) {
    $('.js-store-ios').hide();
  }
});
